package com.pawelbanasik;

public class SimpleExample {

	public static void main(String[] args) {

		int n = 5;
		int result = 0;
		for (int i = 0; i < n; i++) {
			result += (2 * i + 5);

		}
		System.out.println("sum(" + n + ")=" + result);
	}

}
